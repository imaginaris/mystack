//
//  Constants.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation

enum Constants {
    static let stackOverflowApiURL = "https://api.stackexchange.com"
}

enum ViewControllers {
    enum Identifiers {
        static let questions = "QuestionsViewControllerIdentifier"
        static let questionDetails = "QuestionDetailsViewControllerIdentifier"
    }
    enum Cells {
        static let questionCell = "QuestionCell"
    }
}
