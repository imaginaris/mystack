//
//  URLBuilder.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation

final class URLBuilder {
    
    private var components = URLComponents()
    
    func setScheme(scheme: String) -> Self {
        components.scheme = scheme
        return self
    }
    
    func setHost(host: String) -> Self {
        components.host = host
        return self
    }
    
    func setPath(path: String) -> Self {
        components.path = path
        return self
    }
    
    func appendQueryItem(key: String, value: String) -> Self {
        if components.queryItems == nil {
            components.queryItems = [URLQueryItem]()
        }
        
        components.queryItems?.append(URLQueryItem(name: key, value: value))
        return self
    }
    
    func appendQueryItems(items: [String: String]) -> Self {
        if components.queryItems == nil {
            components.queryItems = [URLQueryItem]()
        }
        
        for item in items {
            components.queryItems?.append(URLQueryItem(name: item.key, value: item.value))
        }
        
        return self
    }
    
    func create() -> URL? {
        return components.url
    }
}
