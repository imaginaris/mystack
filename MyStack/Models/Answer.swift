//
//  Answer.swift
//  MyStack
//
//  Created by Michal Kalinowski on 26/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation

struct Answer: Decodable {
    private enum CodingKeys: String, CodingKey {
        case isAccepted = "is_accepted"
        case id = "answer_id"
        case body
    }
    
    let isAccepted: Bool
    let id: Int
    let body: String
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        isAccepted = try container.decode(Bool.self, forKey: .isAccepted)
        body = try container.decode(String.self, forKey: .body)
    }
}
