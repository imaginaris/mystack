//
//  Question.swift
//  MyStack
//
//  Created by Michal Kalinowski on 23/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation

struct QuestionsResponse: Decodable {
    private enum CodingKeys: String, CodingKey {
        case items
    }
    
    let items: [Question]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        items = try container.decode([Question].self, forKey: .items)
    }
}


struct Question: Decodable {
    private enum CodingKeys: String, CodingKey {
        case title
        case id = "question_id"
        case answerCount = "answer_count"
        case tags
        case ownerName = "display_name"
        case ownerImageURL = "profile_image"
        case link
        case body
        case answers
        case owner
    }
    
    let id: Int
    let answerCount: Int
    let title: String
    let tags: [String]
    let ownerName: String
    let ownerImageURL: URL
    let answers: [Answer]
    let link: URL
    let body: String
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let owner = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .owner)
        
        id = try container.decode(Int.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
        answerCount = try container.decode(Int.self, forKey: .answerCount)
        tags = try container.decode([String].self, forKey: .tags)
        ownerName = try owner.decode(String.self, forKey: .ownerName)
        ownerImageURL = try owner.decode(URL.self, forKey: .ownerImageURL)
        answers = try container.decode([Answer].self, forKey: .answers)
        link = try container.decode(URL.self, forKey: .link)
        body = try container.decode(String.self, forKey: .body)
    }
}
