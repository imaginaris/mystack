//
//  ModuleHub.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation

struct ModuleHub {
    
    let registeredModules: [ModuleType.Type] = [
        QuestionsModule.self
    ]
    
    func moduleForRoute(route: String) -> ModuleType? {
        return registeredModules.first(where: { $0.route == route })?.create()
    }
}
