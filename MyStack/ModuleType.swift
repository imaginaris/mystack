//
//  ModuleType.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation
import UIKit

typealias ModuleParameters = [AnyHashable: Any]

enum ParamaterKeys {
    case subpath
}

enum PresentationMode {
    case none
    case root
    case push
    case modal
}

protocol ModuleType {
    static var route: String { get }
    
    static func create() -> ModuleType
    func open(parameters: ModuleParameters, presentationHandler: (UIViewController, PresentationMode) -> Void)
}
