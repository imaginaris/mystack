//
//  QuestionDetailsPresenter.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation
import UIKit

class QuestionDetailsPresenter {
    
    weak var view: QuestionDetailsViewProtocol!
    let questionData: Question
    
    init(questionData: Question, view: QuestionDetailsViewProtocol) {
        self.questionData = questionData
        self.view = view
    }

    func loadData() {
        let acceptedAnswer = questionData.answers.first(where: { $0.isAccepted })?.body ??
                            "No answer was accepted for this question"
        
        let viewModel = QuestionDetailsViewModel(
            owner: questionData.ownerName,
            tagsString: questionData.tags.joined(separator: ", "),
            question: questionData.body,
            answer: acceptedAnswer,
            questionLink: questionData.link,
            ownerImageURL: questionData.ownerImageURL)
        
        view.presentQuestionData(viewModel: viewModel)
    }
}
