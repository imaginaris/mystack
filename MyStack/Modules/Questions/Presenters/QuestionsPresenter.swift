//
//  QuestionsPresenter.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation

class QuestionsPresenter {
    
    weak var view: QuestionsViewProtocol!
    let service: QuestionSearchService
    
    private let router: Router
    private var queryResults: [Question] = []
    
    init(service: QuestionSearchService, view: QuestionsViewProtocol, router: Router) {
        self.service = service
        self.view = view
        self.router = router
    }
    
    func search(query: String) {
        service.search(query: query, completion: { [weak self] questions in
            self?.queryResults = questions
            self?.view.updateList(cellsData: questions.map({
                return QuestionCellViewModel(title: $0.title, answerCount: $0.answerCount)
            }))
        }, failure: { [weak self] error in
            self?.view.presentError(message: "(\(error.httpCode ?? 0)) " + error.localizedDescription)
        })
    }
    
    func didSelectCell(at indexPath: IndexPath) {
        router.openRoute(route: QuestionsModule.route, parameters: [ParamaterKeys.subpath: "details", "question": queryResults[indexPath.row]])
    }
}
