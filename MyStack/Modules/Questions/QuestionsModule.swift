//
//  QuestionsModule.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation
import UIKit

class QuestionsModule: ModuleType, RouterInjector {
    
    static var route: String = "/question"
    
    static func create() -> ModuleType {
        return QuestionsModule()
    }
    
    func open(parameters: ModuleParameters, presentationHandler: (UIViewController, PresentationMode) -> Void) {
        
        //force unwrap is safe here
        let serviceConfiguration = Configuration(baseUrl: URL(string: Constants.stackOverflowApiURL)!)
        let service = StackOverflowService(configuration: serviceConfiguration)
        
        if parameters[ParamaterKeys.subpath] as? String == "details" {
            guard let questionDetailsViewController = router.mainStoryboard.instantiateViewController(withIdentifier: ViewControllers.Identifiers.questionDetails) as? QuestionDetailsViewController,
                let question = parameters["question"] as? Question else {
                fatalError("")
            }
            let questionDetailsPresenter = QuestionDetailsPresenter(questionData: question, view: questionDetailsViewController)
            questionDetailsViewController.presenter = questionDetailsPresenter
            presentationHandler(questionDetailsViewController, .push)
            
        } else {
            guard let questionsViewController = router.mainStoryboard.instantiateViewController(withIdentifier: ViewControllers.Identifiers.questions) as? QuestionsViewController else {
                fatalError("")
            }
            
            let questionsPresenter = QuestionsPresenter(service: service, view: questionsViewController, router: router)
            questionsViewController.presenter = questionsPresenter
            presentationHandler(questionsViewController, .root)
        }
    }
}
