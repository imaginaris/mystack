//
//  QuestionCell.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation
import UIKit

struct QuestionCellViewModel {
    let title: String
    let answerCount: Int
}

class QuestionCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var answersCountLabel: UILabel!
    
    func setup(viewModel: QuestionCellViewModel) {
        titleLabel.text = viewModel.title
        answersCountLabel.text = "\(viewModel.answerCount)"
    }
}
