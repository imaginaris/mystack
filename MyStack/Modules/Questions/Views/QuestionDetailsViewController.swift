//
//  QuestionDetailsViewController.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation
import UIKit

struct QuestionDetailsViewModel {
    let owner: String
    let tagsString: String
    let question: String
    let answer: String
    let questionLink: URL
    let ownerImageURL: URL
}

protocol QuestionDetailsViewProtocol: class {
    func presentQuestionData(viewModel: QuestionDetailsViewModel)
}

final class QuestionDetailsViewController: UIViewController, QuestionDetailsViewProtocol {
    
    @IBOutlet private weak var ownerLabel: UILabel!
    @IBOutlet private weak var tagsLabel: UILabel!
    @IBOutlet private weak var questionLabel: UILabel!
    @IBOutlet private weak var answerLabel: UILabel!
    @IBOutlet private weak var ownerImageView: UIImageView!
    @IBOutlet private weak var linkTextView: UITextView!
    
    var presenter: QuestionDetailsPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.loadData()
    }
    
    func presentQuestionData(viewModel: QuestionDetailsViewModel) {        
        ownerLabel.text = viewModel.owner
        tagsLabel.text = viewModel.tagsString
        questionLabel.text = ""
        answerLabel.text = ""
        
        loadOwnerImage(url: viewModel.ownerImageURL)
        
        if let data = viewModel.question.data(using: .utf8) {
            let questionString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            questionLabel.attributedText = questionString
        }

        if let data = viewModel.answer.data(using: .utf8) {
            let answerString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            answerLabel.attributedText = answerString
        }
        
        setupLink(url: viewModel.questionLink)
    }
    
    private func loadOwnerImage(url: URL) {
        DispatchQueue.global().async {
            if let imageData = try? Data(contentsOf: url) {
                let image = UIImage(data: imageData)
                DispatchQueue.main.async { [weak self] in
                    self?.ownerImageView.image = image
                }
            }
        }
    }
    
    private func setupLink(url: URL) {
        let linkString = NSMutableAttributedString(string: "Open in browser")
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        let range = NSRange(location: 0, length: linkString.length)
        linkString.addAttribute(.link, value: url, range: range)
        linkString.addAttribute(.paragraphStyle, value: paragraph, range: range)
        linkString.addAttribute(.font, value: UIFont.systemFont(ofSize: 17), range: range)
        linkTextView.attributedText = linkString
        linkTextView.textContainerInset = .zero
    }
}
