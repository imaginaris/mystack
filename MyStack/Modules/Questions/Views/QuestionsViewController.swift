//
//  QuestionsViewController.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation
import UIKit

protocol QuestionsViewProtocol: class {
    func updateList(cellsData: [QuestionCellViewModel])
    func presentError(message: String)
}

final class QuestionsViewController: UITableViewController, QuestionsViewProtocol, UISearchBarDelegate {
    
    var presenter: QuestionsPresenter!
    
    private var elements: [QuestionCellViewModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
    }
    
    //MARK: QuestionsViewProtocol
    
    func updateList(cellsData: [QuestionCellViewModel]) {
        elements = cellsData
        tableView.reloadData()
    }
    
    func presentError(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: TableView
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elements.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ViewControllers.Cells.questionCell, for: indexPath) as? QuestionCell else {
            return UITableViewCell()
        }
        
        let cellModel = elements[indexPath.row]
        cell.setup(viewModel: cellModel)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectCell(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: SearchBar
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        presenter.search(query: searchBar.text ?? "")
    }
}
