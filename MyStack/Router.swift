//
//  Router.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation
import UIKit

class Router {
    
    let moduleHub: ModuleHub
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    private var activeRoute = ""
    
    init(moduleHub: ModuleHub) {
        self.moduleHub = moduleHub
    }
    
    func openRoute(route: String, parameters: ModuleParameters? = nil) {
        activeRoute = route
        
        if let module = moduleHub.moduleForRoute(route: route) {
            handleModule(module: module, parameters: parameters)
        }
    }
    
    private func handleModule(module: ModuleType, parameters: ModuleParameters?) {
        module.open(parameters: parameters ?? [:], presentationHandler: { (controllerToPresent, mode) in
            
            if mode == .modal {
                topViewController()?.present(controllerToPresent, animated: true, completion: nil)
            } else if mode == .root {
                if let window = UIApplication.shared.delegate?.window as? UIWindow {
                    window.rootViewController = UINavigationController(rootViewController: controllerToPresent)
                }
            } else {
                topViewController()?.show(controllerToPresent, sender: nil)
            }
        })
    }
    
    private func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        
        if let tabController = controller as? UITabBarController, let selected = tabController.selectedViewController {
            return topViewController(controller: selected)
        }
        
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        
        return controller
    }
}

protocol RouterInjector {
    var router: Router { get }
}

private let sharedRouter = Router(moduleHub: ModuleHub())

extension RouterInjector {
    var router: Router {
        return sharedRouter
    }
}
