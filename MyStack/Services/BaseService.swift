//
//  BaseService.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation

class BaseService {
    
    let configuration: Configuration
    
    init(configuration: Configuration) {
        self.configuration = configuration
    }
    
    func getJSON<T: Decodable>(fromPath path: String,
                               parameters: [String: String],
                               completion: @escaping (T) -> Void,
                               failure: @escaping (ServiceError) -> Void) {
        
        guard let url = buildURL(path: path, parameters: parameters) else {
            failure(ServiceError(code: .urlError, httpCode: nil, localizedDescription: "URL format error", detailedError: nil))
            return
        }
        
        let session = URLSession(configuration: configuration.sessionConfiguration)
        
        session.dataTask(with: url) { (data, response, error) in
            
            let httpCode = (response as? HTTPURLResponse)?.statusCode
            
            if let error = error {
                failure(ServiceError(code: .externalError,
                                     httpCode: httpCode,
                                     localizedDescription: error.localizedDescription,
                                     detailedError: error))
                return
            }
            guard let data = data else {
                failure(ServiceError(code: .noData,
                                     httpCode: httpCode,
                                     localizedDescription: "no data received",
                                     detailedError: nil))
                return
            }

            do {
                let mappedData = try JSONDecoder().decode(T.self, from: data)
                completion(mappedData)
                
            } catch let jsonError {
                failure(ServiceError(code: .mappingError,
                                     httpCode: httpCode,
                                     localizedDescription: jsonError.localizedDescription,
                                     detailedError: jsonError))
            }
            
        }.resume()
    }
    
    func buildURL(path: String, parameters: [String: String]) -> URL? {
        guard let host = configuration.baseUrl.host,
            let scheme = configuration.baseUrl.scheme else {
            return nil
        }
            
        return URLBuilder()
            .setScheme(scheme: scheme)
            .setHost(host: host)
            .setPath(path: path)
            .appendQueryItems(items:parameters).create()
    }
}
