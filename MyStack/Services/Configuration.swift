//
//  Configuration.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation

struct Configuration {
    
    let baseUrl: URL
    var sessionConfiguration: URLSessionConfiguration
    
    init(baseUrl: URL, timeout: TimeInterval = 60) {
        self.baseUrl = baseUrl
        self.sessionConfiguration = URLSessionConfiguration.default
        self.sessionConfiguration.timeoutIntervalForRequest = timeout
    }
}
