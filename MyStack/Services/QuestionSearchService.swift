//
//  QuestionSearchService.swift
//  MyStack
//
//  Created by Michal Kalinowski on 23/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation

protocol QuestionSearchService {
    func search(query: String, completion: @escaping ([Question]) -> Void, failure: @escaping (ServiceError) -> Void)
}
