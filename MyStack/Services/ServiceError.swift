//
//  ServiceError.swift
//  MyStack
//
//  Created by Michal Kalinowski on 25/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation

enum ServiceErrorCode: Int {
    case externalError = -1
    case noData = -10
    case mappingError = -20
    case urlError = -30
}

struct ServiceError: Error {
    let code: ServiceErrorCode
    let httpCode: Int?
    let localizedDescription: String
    let detailedError: Error?
}
