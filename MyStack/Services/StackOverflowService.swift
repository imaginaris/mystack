//
//  StackOverflowService.swift
//  MyStack
//
//  Created by Michal Kalinowski on 23/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import Foundation

final class StackOverflowService: BaseService {

}

extension StackOverflowService: QuestionSearchService {    
    func search(query: String, completion: @escaping ([Question]) -> Void, failure: @escaping (ServiceError) -> Void) {

        getJSON(fromPath: "/2.2/search", parameters: ["order": "desc",
                                                     "sort": "relevance",
                                                     "site": "stackoverflow",
                                                     "intitle": query,
                                                     "filter": "!)5cCAwGtVtFePC*EB15tiz)5SbLu"],
                completion: { (response: QuestionsResponse) in
                    DispatchQueue.main.async {
                        completion(response.items)
                    }
                }, failure: { error in
                    DispatchQueue.main.async {
                        failure(error)
                    }
                })
    }
}
