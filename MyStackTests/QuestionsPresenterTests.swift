//
//  QuestionsPresenterTests.swift
//  MyStackTests
//
//  Created by Michal Kalinowski on 26/07/2018.
//  Copyright © 2018 Michal Kalinowski. All rights reserved.
//

import XCTest

@testable import MyStack

private class QuestionsVCMock: QuestionsViewProtocol {
    
    var errorWasPresented = false
    var listWasUpdated = false
    var cellsDataSpy: [QuestionCellViewModel] = []

    func updateList(cellsData: [QuestionCellViewModel]) {
        listWasUpdated = true
        cellsDataSpy = cellsData
    }
    
    func presentError(message: String) {
        errorWasPresented = true
    }
}

private class QuestionsServiceMock: QuestionSearchService {
    var responseStub: [Question]?
    var errorStub: ServiceError?
    var searchedQuerySpy = ""

    func search(query: String, completion: @escaping ([Question]) -> Void, failure: @escaping (ServiceError) -> Void) {
        
        searchedQuerySpy = query
        
        if let responseStub = responseStub {
            completion(responseStub)
        } else if let errorStub = errorStub {
            failure(errorStub)
        }
    }
}

private class RouterMock: Router {
    var openedRoute = ""
    var routeParameters: ModuleParameters?
    
    override func openRoute(route: String, parameters: ModuleParameters?) {
        openedRoute = route
        routeParameters = parameters
    }
}

final class QuestionsPresenterTests: XCTestCase {
    
    var presenter: QuestionsPresenter!
    private var mockedVC: QuestionsVCMock!
    private var mockedService: QuestionsServiceMock!
    private var mockedRouter: RouterMock!
    
    override func setUp() {
        super.setUp()
        mockedVC = QuestionsVCMock()
        mockedService = QuestionsServiceMock()
        mockedRouter = RouterMock(moduleHub: ModuleHub())
        presenter = QuestionsPresenter(service: mockedService, view: mockedVC, router: mockedRouter)
    }
    
    override func tearDown() {
        presenter = nil
        mockedVC = nil
        mockedService = nil
        mockedRouter = nil
        
        super.tearDown()
    }
    
    func test_SearchResultsAreMappedCorrectly() {
        mockedService.responseStub = [Question(id: 1), Question(id: 2), Question(id: 3)]
        presenter.search(query: "query")
        
        XCTAssertEqual(mockedService.searchedQuerySpy, "query")
        XCTAssertNotNil(mockedService.responseStub)
        compare(questions: mockedService.responseStub ?? [], with: mockedVC.cellsDataSpy)
    }
    
    func test_ErrorIsPresentedCorrectly() {
        mockedService.errorStub = ServiceError(code: .externalError, httpCode: 500, localizedDescription: "desc", detailedError: nil)
        presenter.search(query: "query")
        
        XCTAssertTrue(mockedVC.errorWasPresented)
    }
    
    func test_ShowDetailsWithProperQuestionAfterTapOnCell() {
        mockedService.responseStub = [Question(id: 1), Question(id: 2), Question(id: 3)]
        presenter.search(query: "query")
        presenter.didSelectCell(at: IndexPath(row: 1, section: 0))
        
        XCTAssertEqual(mockedRouter.openedRoute, QuestionsModule.route)
        XCTAssertNotNil(mockedRouter.routeParameters)
        let subpath = mockedRouter.routeParameters?[ParamaterKeys.subpath] as? String
        let question = mockedRouter.routeParameters?["question"] as? Question
        XCTAssertEqual(subpath, "details")
        XCTAssertNotNil(question)
        XCTAssertEqual(question?.id, 2)
    }
    
    private func compare(questions: [Question], with viewModels: [QuestionCellViewModel]) {
        XCTAssertEqual(questions.count, viewModels.count)

        for (idx, question) in questions.enumerated() {
            XCTAssertEqual(question.answerCount, viewModels[idx].answerCount)
            XCTAssertEqual(question.title, viewModels[idx].title)
        }
    }
}

extension Question {
    init(id: Int) {        
        self.id = id
        self.answerCount = 10 + id
        self.answers = []
        self.body = ""
        self.link = URL(string: "about:blank")!
        self.ownerImageURL = URL(string: "about:blank")!
        self.ownerName = "name"
        self.tags = []
        self.title = "title \(id)"
    }
}
